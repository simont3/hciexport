Introduction
============

Each and every IT system needs some sort of backup/restore, and Hitachi Content Intelligence (HCI)
is not different. On one hand, system configuration needs to be secured, but also Workflow details need
to be kept safe, and eventually even versioned. Indexes might need backup, as well - even if it's often
possible to re-index the data sources.

The **hciexport** tool wraps some of HCIs features related to these needs
into a convenient automation tool.

Features included are:

    *   Configuration export:

        *   Export a complete Workflow bundle (all Workflow-related *configuration* like DataSources,
            Pipelines, Indexes, Content Classes, Workflows)

        *   Export the complete System configuration (all system configuration, plugins, as well as the
            Workflow configuration)

    *   Control backup and restore of Indexes:

        *   list existing indexes
        *   start index backup, get backup status, list backup iterations, delete backups
        *   start index restore, get restore status

            ..  Note::

                *hciexport* is **not performing** backup or restore - it is **triggering** HCI to do so.

                It's a pre-requisite to prepare HCI for backup/restore of Indexes, as described in the
                HCI documentation!

