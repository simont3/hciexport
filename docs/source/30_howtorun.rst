Use
===

Prerequisites
-------------

*   **hciexport** has been installed in a Python virtual environment
*   The virtual environment has been activated
*   A configuation file (hciexport.cnf) has been created

Return Codes
------------

*hciexport* emits these return codes on exit:

    *   0 - success
    *   1 - fail
    *   2 - cli parameter issue

General Syntax
--------------

::

    hciexport -C hciexport.cnf -h
    usage: hciexport [-h] [--version] -C CONFIGFILE \
                     {export,index,backup,restore} ...

    options:
      -h, --help            show this help message and exit
      --version             show program's version number and exit
      -C CONFIGFILE         configuration file

    commands:
      {export,index,backup,restore}
        export              export system configuration or workflow bundle
        index               list indexes
        backup              index backup functions
        restore             restore an index


Configuration Export
--------------------

::

    hciexport -C hciexport.cnf export -h
    usage: hciexport export [-h] [-o OUTFILE] {system,workflows}

    positional arguments:
      {system,workflows}  the task within the config file

    options:
      -h, --help          show this help message and exit
      -o OUTFILE          the file to write the export to (set default in config file)


Export the Workflow configuration
+++++++++++++++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf export workflows
        retrieving workflows:                   OK (7)
        retrieving dataSources:                 OK (6)
        retrieving pipelines:                   OK (14)
        retrieving indexes:                     OK (6)
        retrieving contentClasses:              OK (3)
        exporting workflows:                    OK (hciexport.workflow.bundle)


Export the System configuration
+++++++++++++++++++++++++++++++

::

    (.venv) $ hciexport.py -C hciexport.cnf export system
        retrieving business objects:            OK
        retrieving security settings:           OK
        retrieving identity providers:          OK
        retrieving groups:                      OK
        retrieving roles:                       OK
        retrieving notification rules:          OK
        retrieving client certificates:         OK
        retrieving system certificates:         OK
        retrieving plugin bundles:              OK
        building package:                       OK
        exporting system config:                OK (hciexport.system.package)


Indexes
-------

::

    hciexport -C hciexport.cnf index -h
    usage: hciexport index [-h] [-v]

    options:
      -h, --help  show this help message and exit
      -v          verbose, include index statistics


List existing Indexes
+++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf index -v
                   index |         docs |         size | IPL | shards
    -------------------- + ------------ + ------------ + --- + ------
               email_idx |       244192 |    10.55 GiB |   2 |     18
            thorsten_idx |       122096 |     5.27 GiB |   1 |     18


Index Backup
------------

::

    hciexport -C hciexport.cnf backup -h
    usage: hciexport backup [-h] {start,status,list,delete} ...

    options:
      -h, --help            show this help message and exit

    backup subcommands:
      {start,status,list,delete}
        start               start a backup
        status              get backup status
        list                list backups
        delete              delete backups


Start an Index Backup
+++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf backup start thorsten_idx
    Backup of "thorsten_idx" started

Get Index Backup status
+++++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf backup status thorsten_idx
    thorsten_idx: COMPLETED


Get a list of Backup iterations
+++++++++++++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf backup list thorsten_idx
    backupId | start Timestamp
    -------- + ------------------------
           0 - 2024-03-19T19:06:47.451Z
           1 - 2024-03-19T19:08:45.008Z


Delete a Backup
+++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf backup delete --full_delete thorsten_idx
    thorsten_idx: delete ({'deleteAllBackups': True}) successful



Index Restore
-------------

::

    hciexport.py -C .\hciexport.cnf restore -h
    usage: hciexport.py restore [-h] {start,status} ...

    options:
      -h, --help      show this help message and exit

    restore subcommands:
      {start,status}
        start         start a restore
        status        get restore status


Start an Index Restore
++++++++++++++++++++++

::

    (.venv) $ hciexport -C hciexport.cnf restore start email_idx thorsten_idx -1
    email_idx (-1) -> thorsten_idx: restore started


Get Index Restore Status
++++++++++++++++++++++++

::

    (.venv) $  hciexport -C hciexport.cnf restore status thorsten_idx
    thorsten_idx: COMPLETED

