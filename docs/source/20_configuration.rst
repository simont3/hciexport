Configuration
=============

Configuration data needed by **hciexport** has to be configured in a
configuration file. Use the template below to create it.

..  Note::

    If the specified config file does not exist, *hciexport* will offer to write a
    template configuration file with the given name. Just edit that one and then run *hciexport*
    again!

Template::

    [HCI]
    # The HCI's FQDN, like myhci.mydomain.com
    HCIFQDN = <HCI FQDN>
    ADMINAPIPORT = 8000
    SEARCHAPIPORT = 8888
    WORKFLOWAPIPORT = 8888
    USER = <user>
    PASSWORD = <it's password>
    # the REALM is either 'local' for the built-in admin user or whatever
    # REALM was configured when integrating HCI with an external IDP
    REALM = <realm>
    GRANTTYPE = password
    CLIENTSECRET = hciexport
    CLIENTID = hciexport

    [system]
    # a default filename of your choice
    OUTPUTFILE = hciexport.system.package

    [workflows]
    # a default filename of your choice
    OUTPUTFILE = hciexport.workflow.bundle

..  versionchanged:: 0.4.0
