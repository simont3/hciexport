Installation
============

Using a Python virtual environment
----------------------------------

*   Create a Python 3.8 (or newer) virtual environment and activate it::

        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Install the **hciexport** package::

        (.venv) $ pip install -U setuptools pip wheel
        (.venv) $ pip install hciexport

*   Create a configuration file as described in the *configuration* chapter

*   Now you can run **hciexport** this way::

        (.venv) $ hciexport -C <config file> --version

..  Note::

    Remember, every time you want to run **hciexport**, you need to
    activate the virtual environment before!


For experts: build an executable
--------------------------------

Pre-requisites:

    *   Python >= 3.8 and < 3.12 installed (*Nuitka doesn't support Python 3.12 at the time of writing this*)
    *   git installed
    *   On Linux, a dev environment (C-compiler and toolchain)

*   Set up and activate a Python virtual environment::

        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Clone the hcpinsight repository::

        (.venv) $ git clone https://gitlab.com/simont3/hciexport.git
        (.venv) $ cd hciexport
        (.venv) $ pip install -r pip-requirements-dev.txt

*   Compile::

        (.venv) $ cd src
        (.venv) $ python -m nuitka --onefile hciexport.py

    This will produce an executable binary that can be moved to a convenient folder (*/usr/local/bin*, likely)

..  Note::

    The procedure above will work on Windows, too. It doesn't require a C-compiler to be present, as Nuitka
    will offer to install what is needed. Just allow it to do so.

    Unfortunately, it turns out that the executable built is triggering an alarm (and its removal) by MS Defender
    Anti-Virus. This is a false alarm, but that doesn't help :-(

    This can be worked around by running the compiler this way::

        (.venv) > python -m nuitka --standalone hciexport.py

    In this case, there will be a folder *hciexport.dist*, including the executable *hciexport.exe*, along a few other
    library files. All of these are needed, and *hciexport.exe* needs to be located along with them. Nevertheless,
    as you move the entire folder, it's fine. Don't forget to add the folder to %PATH%.

