Release History
===============

**0.4.0 2024-03-20**

    **Warning**: the configuration file structure has changed!

    added Index backup and restore functionality

**0.3.2 2020-10-18**

    installer issue fixed

**0.3.0 2020-10-18**

    added documentation, package ready to publish

**0.2.0 2020-10-14**

    export system config

**0.1.0 2020-10-04**

    export workflows
